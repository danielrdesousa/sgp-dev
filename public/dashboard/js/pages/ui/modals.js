$(function () {
    $('.add_interested').on('click', function () {
        var color = $(this).data('color');
        $('#mdModal .modal-content').removeAttr('class').addClass('modal-content modal-col-' + color);
        $('#mdModal').modal('show');

        $('.search_interested').val('');
    });
});

