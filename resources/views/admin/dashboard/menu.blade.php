<div class="menu">
    <ul class="list">
        <li class="header">MENU DE NAVEGAÇÃO</li>
        <div style="display: none;">
            <li class="{{ Request::is('*') ? 'active' : '' }}">

            </li>
        </div>
        <li class="{{ Request::is('/') ? 'active' : '' }}">
            <a href="/" class="{{ Request::is('/') ? 'active' : '' }}">
                <i class="material-icons">home</i>
                <span>Página Inicial</span>
            </a>
        </li>
        <li class="{{ Request::is('users*') ? 'active' : '' }}">
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">local_library</i>
                <span>Gerenciar Usuários</span>
            </a>
            <ul class="ml-menu">
                <li class="{{ Request::is('users') ? 'active' : '' }}">
                    <a href="{{ route('user.index') }}">
                        <span>Listagem de Usuários</span>
                    </a>
                </li>            
                <li class="{{ Request::is('users/register') ? 'active' : '' }}">
                    <a href="{{ route('user.create') }}">
                        <span>Cadastrar Usuário</span>
                    </a>
                </li>
            </ul>
        </li>
        
        <!-- processos -->
        <li class="{{ Request::is('processes') ? 'active' : '' }} {{ Request::is('processes/register') ? 'active' : '' }}">
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">library_books</i>
                <span>Gerenciar Processos</span>
            </a>
            <ul class="ml-menu">
                <li class="{{ Request::is('processes') ? 'active' : '' }}">
                    <a href="{{ route('process.index') }}">
                        <span>Listagem de Processo</span>
                    </a>
                </li>
                <li class="{{ Request::is('processes/register') ? 'active' : '' }}">
                    <a href="{{ route('process.create') }}">
                        <span>Cadastrar Processo</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <span>Consultar Processo</span>
                    </a>
                </li>
            </ul>
        </li>          
        
        <!-- interessados -->
        <li>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">group</i>
                <span>Interessados</span>
            </a>
            <ul class="ml-menu">
                <li class="{{ Request::is('processes/interested') ? 'active' : '' }}">
                    <a href="javascript:void(0);">
                        <span>Listagem de Interessados</span>
                    </a>
                </li>
                <li class="{{ Request::is('processes/interested/register') ? 'active' : '' }}">
                    <a href="javascript:void(0);">
                        <span>Cadastrar Interessado</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0);">
                        <span>Consultar Interessado</span>
                    </a>
                </li>
            </ul>
        </li>
        
        <!-- classificações -->
        <li class="{{ Request::is('processes/classifications*') ? 'active' : '' }}">
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">list</i>
                <span>Classificações</span>
            </a>
            <ul class="ml-menu">
                <li class="{{ Request::is('processes/classifications') ? 'active' : '' }}">
                    <a href="{{ route('classification.index') }}">
                        <span>Listagem de Classificações</span>
                    </a>
                </li>
                <li class="{{ Request::is('processes/classifications/register') ? 'active' : '' }}">
                    <a href="{{ route('classification.create') }}">
                        <span>Cadastrar Classificação</span>
                    </a>
                </li>
            </ul>
        </li>


    </ul>
</div>