@extends('admin.dashboard.blank')

@section('title', 'Cadastrar Usuário')

@push('styles')
    <link href="{{ asset('dashboard/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet">
@endpush


@section('content')
<section class="content">
    <ol class="breadcrumb breadcrumb-col-pink align-left">
        <li>
            <a href="{{ route('homepage') }}">
                <i class="material-icons">home</i> Home
            </a>
        </li>
        <li class="active">
            <i class="material-icons">create</i> Cadastrar Usuário
        </li>
    </ol>

    <div class="container-fluid">
        <div class="block-header">
            <h2>
            	Cadastrar Usuário
	            <small>Preencha <b>TODOS</b> os campos corretamente</small>
            </h2>
        </div>

        <!-- will be used to show any messages -->
        @include('admin.dashboard.alerts')

        <form role="form" method="POST" id="form_validation"  action="{{ route('user.store') }}" novalidate="novalidate" enctype="multipart/form-data">
            {!! csrf_field() !!}

        <!-- Form register User -->
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="card">
                    <div class="header">
                        <h2>Informações Pessoais</h2>
                    </div>
                    <div class="body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" value="{{old('username')}}">
                                <label class="form-label">Nome Completo</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="email" class="form-control" name="email">
                                <label class="form-label">Email</label>
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea name="description" cols="30" rows="5" class="form-control no-resize"></textarea>
                                <label class="form-label">Observações</label>
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="password" class="form-control" name="password">
                                <label class="form-label">Senha</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <div class="card">
                    <div class="header">
                        <h2>
                            Informações de Login
                        </h2>
                    </div>
                    <div class="body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <select class="form-control show-tick">
                                    <option value="">-- Nivel de Acesso --</option>
                                    <option value="1">Administrador</option>
                                    <option value="2">Colaborador</option>
                                    <option value="3">Comum</option>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary m-t-15 waves-effect">
            <i class="material-icons">save</i>
            <span>Cadastrar Usuário</span>
        </button>
    </form>

	</div>
</section>
@endsection


@push('scripts')
    <script src="{{ asset('dashboard/plugins/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
@endpush