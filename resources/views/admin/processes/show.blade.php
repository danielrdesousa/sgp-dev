@extends('admin.dashboard.blank')

@section('title', 'Listagem de Usuários')

@push('styles')

@endpush


@section('content')
<section class="content">
    <ol class="breadcrumb breadcrumb-col-pink align-left">
        <li>
            <a href="{{ route('homepage') }}">
                <i class="material-icons">home</i> Home
            </a>
        </li>
        <li>
            <a href="{{ route('process.index') }}">
                <i class="material-icons">library_books</i> Listagem de Processos
            </a>
        </li>
        <li class="active">
            <i class="material-icons">view_headline</i> Detalhes
        </li>
    </ol>

    <div class="container-fluid">
        <div class="block-header">
            <h2>
            	Detalhes do Processo
	            <small>Exibindo <b>TODAS</b> as informações referentes ao processo</small>
            </h2>
        </div>

        <!-- will be used to show any messages -->
        @include('admin.dashboard.alerts')
        
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box-2 bg-light-blue hover-zoom-effect">
                    <div class="icon">
                        <i class="material-icons">date_range</i>
                    </div>
                    <div class="content">
                        <div class="text">PRÓXIMO ALERTA</div>
                        <div class="number">
                            <?php
                                if($process->date_next_stage == ''){
                                    echo $process->date_next_stage;
                                }else{
                                    echo date('d/m/Y', strtotime($process->date_next_stage)); 
                                }
                            ?>
                            <!-- {{ $process->date_next_stage }} -->

                        </div>
                    </div>
                </div>
            </div>            
        </div>

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                        	PROCESSO Nº: {{ $process->process_number }}
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <p>
                            date_create:

                            <?php echo date('d/m/Y', strtotime($process->date_create)) ?>

                        </p>                 
                        <p>
                            current_guard_stage: {{ $process->current_guard_stage }}
                        </p>
                        <p>
                            classifications_id: {{ $process->classifications_id }}
                        </p>                        
                        <p>
                            interested: {{ $process->interested }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
	</div>
</section>
@endsection


@push('scripts')

@endpush