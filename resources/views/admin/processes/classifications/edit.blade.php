@extends('admin.dashboard.blank')

@section('title', 'Cadastrar Classificação')

@push('styles')
    <link href="{{ asset('dashboard/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet">
@endpush


@section('content')
<style>
    .upper{
        text-transform: uppercase;
    }
</style>

<section class="content">
    <ol class="breadcrumb breadcrumb-col-pink align-left">
        <li>
            <a href="{{ route('homepage') }}">
                <i class="material-icons">home</i> Home
            </a>
        </li>
        <li>
            <a href="{{ route('classification.index') }}">
                <i class="material-icons">list</i> Listagem de Classificações
            </a>
        </li>
        <li class="active">
            <i class="material-icons">mode_edit</i> Editar Classificação
        </li>
    </ol>

    <div class="container-fluid">
        <div class="block-header">
            <h2>
            	Editar Classificação
	            <small>Preencha <b>TODOS</b> os campos corretamente</small>
            </h2>
        </div>

        <!-- will be used to show any messages -->
        @include('admin.dashboard.alerts')

        <form role="form" method="POST" action="{{ route('classification.update', $classification->id) }}" enctype="multipart/form-data">
            {!! csrf_field() !!}

        <!-- Form register User -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>INFORMAÇÕES BÁSICAS</h2>
                    </div>
                    <div class="body">


                        <div class="row clearfix">
                            <div class="col-md-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control upper" name="cod" value="{{ $classification->cod }}">
                                        <label class="form-label">Código (Identificador)</label>
                                    </div>
                                    <div class="help-info">Ex: 024.155</div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control upper" name="subject" value="{{ $classification->subject }}">
                                        <label class="form-label">Assunto</label>
                                    </div>
                                    <div class="help-info">Ex: SALÁRIO MATERNIDADE</div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="card">
                    <div class="header">
                        <h2>
                            PRAZOS DE GUARDA
                        </h2>
                    </div>
                    <div class="body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control upper" name="current_phase" value="{{ $classification->current_phase }}">
                                <label class="form-label">Fase Corrente</label>
                            </div>
                            <div class="help-info">Ex: ENQUANTO VIGORA</div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control upper" name="intermediate_phase" value="{{ $classification->intermediate_phase }}">
                                <label class="form-label">Fase Intermediária</label>
                            </div>
                            <div class="help-info">Ex: 5 ANOS</div>
                        </div>
                        <div class="form-group form-float">
                            <div class="form-line">
                                <input type="text" class="form-control upper" name="final_destination" value="{{ $classification->final_destination }}">
                                <label class="form-label">Destinação Final</label>
                            </div>
                            <div class="help-info">Ex: GUARDA PERMANENTE</div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <div class="card">
                    <div class="header">
                        <h2>
                            OBSERVAÇÕES
                        </h2>
                    </div>
                    <div class="body">
                        <div class="form-group form-float">
                            <div class="form-line">
                                <textarea name="observations" cols="30" rows="7" class="form-control no-resize upper" aria-required="true">{{ $classification->observations }}</textarea>
                                <label class="form-label">Descrição</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-primary m-t-15 waves-effect">
            <i class="material-icons">save</i>
            <span>Salvar Alterações</span>
        </button>
    </form>

	</div>
</section>
@endsection


@push('scripts')
    <script src="{{ asset('dashboard/plugins/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-select/js/bootstrap-select.js') }}"></script>
@endpush