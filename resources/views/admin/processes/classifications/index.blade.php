@extends('admin.dashboard.blank')

@section('title', 'Listagem de Classificações')

@push('styles')
	<!-- JQuery DataTable Css -->
    <link href="{{ asset('dashboard/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush


@section('content')
<section class="content">
    <ol class="breadcrumb breadcrumb-col-pink align-left">
        <li>
            <a href="{{ route('homepage') }}">
                <i class="material-icons">home</i> Home
            </a>
        </li>
        <li class="active">
            <i class="material-icons">library_books</i> Listagem de Classificações
        </li>
    </ol>

    <div class="container-fluid">
        <div class="block-header">
            <h2>
            	Listagem de Classificações
	            <small>Exibindo <b>TODOS</b> as classificações cadastradas</small>
            </h2>
        </div>

        <!-- will be used to show any messages -->
        @include('admin.dashboard.alerts')

        <!-- Basic Examples -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                        	CLASSIFICAÇÕES CADASTRADAS
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>COD</th>
                                        <th>Assunto</th>
                                        <th>Fase Corrente</th>
                                        <th>Fase Intermediária</th>
                                        <th>Destinação Final</th>
                                        <th style="width: 60px;">Ações</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>COD</th>
                                        <th>Assunto</th>
                                        <th>Fase<br> Corrente</th>
                                        <th>Fase Intermediária</th>
                                        <th>Destinação Final</th>
                                        <th style="width: 60px;">Ações</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($classifications as $classification)
                                        <tr>
                                            <td>{{ $classification->cod }}</td>
                                            <td>{{ $classification->subject }}</td>
                                            <td>{{ $classification->current_phase }}</td>
                                            <td>{{ $classification->intermediate_phase }}</td>
                                            <td>{{ $classification->final_destination }}</td>
                                            <td>
                                                <div class="btn-group btn-group-justified" role="group" aria-label="Justified button group">
                                                    <a href="{{ route('classification.edit', $classification->id) }}" class="btn bg-blue-grey waves-effect" role="button">
                                                        <i class="material-icons">mode_edit</i>
                                                    </a>
                                                    <a href="{{ route('classification.destroy', $classification->id) }}" class="btn bg-red waves-effect" role="button" data-type="cancel">
                                                        <i class="material-icons">delete_forever</i>
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->
	</div>
</section>
@endsection


@push('scripts')
    <!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('dashboard/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
@endpush

@push('custom-script')
    <script src="{{ asset('dashboard/js/pages/tables/jquery-datatable.js') }}"></script>
@endpush