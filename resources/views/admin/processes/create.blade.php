@extends('admin.dashboard.blank')

@section('title', 'Cadastrar Processo')

@push('styles')
    <link href="{{ asset('dashboard/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
@endpush


@section('content')
<style>
    #modalInterested{
        margin-top: 15%;
    }
    #modalInterested h4{
        text-align: center;
    }
    #modalInterested a{
        color: #000;
        text-decoration: none;
        font-weight: bold;
    }
    .upper{
        text-transform: uppercase;
    }
</style>

<section class="content">
    <ol class="breadcrumb breadcrumb-col-pink align-left">
        <li>
            <a href="{{ route('homepage') }}">
                <i class="material-icons">home</i> Home
            </a>
        </li>
        <li class="active">
            <i class="material-icons">create</i> Cadastrar Processo
        </li>
    </ol>

    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Cadastrar Processo
                <small>Preencha <b>TODOS</b> os campos corretamente</small>
            </h2>
        </div>

        <!-- will be used to show any messages -->
        @include('admin.dashboard.alerts')

        <form role="form" method="POST"  action="{{ route('process.store') }}" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <!-- campos de teste -->
            <input type="hidden" name="interested[]">
            <input type="hidden" name="classification_id">

            <div class="row clearfix">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>INFORMAÇÕES BÁSICAS</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control upper" name="process_number">
                                            <label class="form-label">Nº PROCESSO</label>
                                        </div>
                                        <div class="help-info">Ex: 23067.003732/2018-10</div>
                                    </div>                                    
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control upper" name="subject">
                                            <label class="form-label">ASSUNTO</label>
                                        </div>
                                        <div class="help-info">Ex: PASTA DO SERVIDOR</div>
                                    </div>                                    
                                </div>                               
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>DATA DE CRIAÇÃO</h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control upper" name="date_month">
                                            <label class="form-label">MÊS</label>
                                        </div>
                                        <div class="help-info">Ex: JANEIRO</div>
                                    </div>                                
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control upper" name="date_year">
                                            <label class="form-label">ANO</label>
                                        </div>
                                        <div class="help-info">Ex: 2008</div>
                                    </div>                               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>INTERESSADOS NO PROCESSO</h2>
                            <ul class="header-dropdown m-r-0">
                                <li class="add_interested" data-toggle="modal" data-target="#modalInterested">
                                    <a href="javascript:void(0);">
                                        <i class="material-icons">person_add</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <p>Este processo ainda não possui interessados...</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>CLASSIFICAÇÃO DO PROCESSO</h2>
                            <ul class="header-dropdown m-r-0">
                                <li class="add_interested" data-toggle="modal" data-target="#modalInterested">
                                    <a href="javascript:void(0);">
                                        <i class="material-icons">format_indent_increase</i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <p>Nenhuma classificação vinculada a este processo...</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>OBSERVAÇÕES</h2>
                        </div>
                        <div class="body">
                            <div class="form-group form-float">
                                <div class="form-line">

                                    <textarea name="observations" cols="30" rows="7" class="form-control no-resize upper" aria-required="true"></textarea>
                                    <label class="form-label">Descrição</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary m-t-15 waves-effect" style="margin-bottom: 20px">
                <i class="material-icons">save</i>
                <span>Cadastrar Processo</span>
            </button>
        </form>

    </div>
</section>

<!-- SEARCH INTERESTED -->
<div class="modal fade" id="modalInterested" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">SELECIONE UM USUÁRIO</h4>
            </div>
            <div class="modal-body">
                <p>
                    Todos os usuários listados aqui foram cadastrados previamente nesta plataforma, caso não encontre o usuário desejado <a href="javascript:void(0);">CLIQUE AQUI</a> para realizar seu cadastro.
                </p>
                <hr>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">FECHAR</button>
            </div>
        </div>
    </div>
</div>
@endsection


@push('scripts')
    <script src="{{ asset('dashboard/plugins/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('dashboard/js/create-processes.js') }}"></script>
    <script src="{{ asset('dashboard/js/pages/ui/modals.js') }}"></script>
@endpush