@extends('admin.dashboard.blank')

@section('title', 'Listagem de Processos')

@push('styles')
	<!-- JQuery DataTable Css -->
    <link href="{{ asset('dashboard/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
@endpush


@section('content')
<section class="content">
    <ol class="breadcrumb breadcrumb-col-pink align-left">
        <li>
            <a href="{{ route('homepage') }}">
                <i class="material-icons">home</i> Home
            </a>
        </li>
        <li class="active">
            <i class="material-icons">library_books</i> Listagem de Processos
        </li>
    </ol>

    <div class="container-fluid">
        <div class="block-header">
            <h2>
            	Listagem de Processos
	            <small>Exibindo apenas os <b>últimos 2.000</b> os processos</small>
            </h2>
        </div>

        <!-- will be used to show any messages -->
        @include('admin.dashboard.alerts')

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                        	ÚLTIMOS PROCESSOS CADASTRADOS
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th>Processo</th>
                                        <th>Interessado</th>
                                        <th>Assunto</th>
                                        <th>CPF/CNPJ</th>
                                        <th>Prazo de Guarda</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>Processo</th>
                                        <th>Interessado</th>
                                        <th>Assunto</th>
                                        <th>CPF/CNPJ</th>
                                        <th>Prazo de Guarda</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                    @foreach($processes as $process)
                                    <tr>
                                        <td>
                                            <a href="{{ route('process.show', $process->id) }}">{{ $process->process_number }}</a>
                                        </td>
                                        <td>
                                            {{ $process->interested }}
                                        </td>
                                        <td>
                                            {{ $process->subject }}
                                        </td>
                                        <td>
                                            @if ($process->cpf == 0)
                                                {{ $process->cnpj }}
                                            @else
                                                {{ $process->cpf }}
                                            @endif
                                        </td>
                                        <td>
                                            @if ($process->current_guard_stage == 0)
                                                FINALIZADO
                                            @elseif($process->current_guard_stage == 1)
                                                FASE CORRENTE
                                            @elseif($process->current_guard_stage == 2)
                                                FASE INTERMEDIÁRIA
                                            @else
                                                OUTRA FASE
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

	</div>
</section>
@endsection


@push('scripts')
    <script src="{{ asset('dashboard/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
@endpush

@push('custom-script')
    <script src="{{ asset('dashboard/js/pages/tables/jquery-datatable.js') }}"></script>
@endpush