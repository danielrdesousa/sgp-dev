@extends('admin.dashboard.blank')

@section('title', 'Visualizar Perfil')

@push('styles')
    <link href="{{ asset('dashboard/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet">
    <link href="{{ asset('dashboard/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet">
@endpush

@section('content')

    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2>VISUALIZAR PERFIL</h2>
            </div>
            @include('admin.dashboard.alerts')

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="card">
                            <div class="header">
                                <h2>Foto</h2>
                                <ul class="header-dropdown m-r--5">
                                    <li class="dropdown">
                                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <i class="material-icons">more_vert</i>
                                        </a>
                                        <ul class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0);">Mudar</a></li>
                                            <li><a href="javascript:void(0);">Excluir</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="body">
                                <div class="image">
                                    <img class="img-responsive" src="https://365psd.com/images/istock/previews/9353/93539553-flat-vector-avatar-face-character-person-portrait-user-icon.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="card">
                            <div class="header">
                                <h2>Informações Pessoais</h2>
                            </div>
                            <div class="body">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="name" value="{{ Auth::User()->name }}" readonly>
                                        <label class="form-label">Nome</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="e-mail" value="{{ Auth::User()->email }}" readonly>
                                        <label class="form-label">E-mail</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="career" value="{{ Auth::User()->career }}" readonly>
                                        <label class="form-label">Cargo</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control" name="cpf" value="{{ Auth::User()->cpf }}" readonly>
                                        <label class="form-label">CPF</label>
                                    </div>
                                </div>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="date" class="form-control" name="date_birth" value="{{ Auth::User()->date_birth }}" readonly>
                                        <label class="form-label">Data de Nascimento</label>
                                    </div>
                                </div><!-- 
                                <button type="button" class="btn bg-green waves-effect" onclick="window.location='{{ route('profile.edit') }}'">
                                    <i class="material-icons">mode_edit</i>
                                    <span>Editar</span>
                                </button> -->
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>
@endsection

@push('scripts')
    <script src="{{ asset('dashboard/plugins/jquery-inputmask/jquery.inputmask.bundle.js') }}"></script>
    <script src="{{ asset('dashboard/js/create-processes.js') }}"></script>
@endpush
