@extends('admin.dashboard.login-register')

@section('content')
<form id="forgot_password" method="POST">
    <div class="msg">
        Enter your email address that you used to register. We'll send you an email with your username and a
        link to reset your password.
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">email</i>
        </span>
        <div class="form-line">
            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{ old('email') }}" required autofocus>

            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">
        {{ __('Send Password Reset Link') }}
    </button>

    <div class="row m-t-20 m-b--5 align-center">
        <a href="{{ route('login') }}">Sign In!</a>
    </div>
</form>
@endsection
