@extends('admin.dashboard.login-register')

@section('content')
<form method="POST" action="{{ route('login') }}">
    @csrf
    <div class="msg">Sign in to start your session</div>


    @if ($errors->has('email'))
        <div class="alert bg-pink alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ $errors->first('email') }}
        </div>
    @endif

    @if ($errors->has('password'))
        <div class="alert bg-pink alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ $errors->first('password') }}
        </div>
    @endif

    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">email</i>
        </span>
        <div class="form-line">
            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" value="{{ old('email') }}" placeholder="Email Address" required autofocus>
        </div>
    </div>



    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">lock</i>
        </span>

        <div class="form-line">
            <input type="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-8 p-t-5">
            <input type="checkbox" name="remember" id="rememberme" class="filled-in chk-col-pink" {{ old('remember') ? 'checked' : '' }}> 
            <label for="rememberme">{{ __('Remember Me') }}</label>
        </div>
        <div class="col-xs-4">
            <button class="btn btn-block bg-pink waves-effect" type="submit">{{ __('Login') }}</button>
        </div>
    </div>

    <div class="row m-t-15 m-b--20">
        <div class="col-xs-6">
            <a href="{{ route('register') }}">Register Now!</a>
        </div>
        <div class="col-xs-6 align-right">
            <a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
        </div>
    </div>
</form>
@endsection
