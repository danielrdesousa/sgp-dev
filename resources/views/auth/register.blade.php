@extends('admin.dashboard.login-register')

@section('content')
<form id="sign_up" method="POST">
    <div class="msg">Registration Form</div>

    @if ($errors->has('email'))
        <div class="alert bg-pink alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ $errors->first('email') }}
        </div>
    @endif

    @if ($errors->has('password'))
        <div class="alert bg-pink alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ $errors->first('password') }}
        </div>
    @endif

    @if ($errors->has('name'))
        <div class="alert bg-pink alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            {{ $errors->first('name') }}
        </div>
    @endif

    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">person</i>
        </span>
        <div class="form-line">
            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Username "  value="{{ old('name') }}"  required autofocus>
        </div>
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">email</i>
        </span>
        <div class="form-line">
            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"  value="{{ old('email') }}" placeholder="Email Address" required>
        </div>
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">lock</i>
        </span>
        <div class="form-line">
            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" minlength="6" placeholder="Password" required>
        </div>
    </div>
    <div class="input-group">
        <span class="input-group-addon">
            <i class="material-icons">lock</i>
        </span>
        <div class="form-line">
            <input type="password" id="password-confirm" class="form-control" name="password_confirmation" minlength="6" placeholder="Confirm Password" required>
        </div>
    </div>

    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">{{ __('Register') }}</button>

    <div class="m-t-25 m-b--5 align-center">
        <a href="{{ route('login') }}">Are you already registered??</a>
    </div>
</form>
@endsection


