<?php

use App\Models\interestedPhysicals;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class InterestedPhysicalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('interested_physicals')->insert([
		    'name' 	=> 'DANIEL RODRIGUES DE SOUSA',
		    'career' => 'ESTUDANTE',
		    'cpf' => '61051001323',
		    'date_birth' => '1996-05-22',
		    'registration_numbers' => '',
		    'observations' => '',
	        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
