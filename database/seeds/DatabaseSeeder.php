<?php

use Illuminate\Database\Seeder;
use App\Models\InterestedPhysical;
use App\Models\Classification;
use App\Models\Process;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UserTableSeeder::class);
        $this->call(ClassificationTableSeeder::class);
        $this->call(InterestedPhysicalsTableSeeder::class);
    }
}
