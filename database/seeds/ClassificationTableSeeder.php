<?php

use Illuminate\Database\Seeder;
use App\Models\Classification;

class ClassificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Classification::create([
            'cod'		=> '000',
            'subject' 	=> 'ADMINISTRAÇÃO GERAL'
        ]);

        Classification::create([
            'cod'		=> '001',
            'subject' 	=> 'MODERNIZAÇÃO E REFORMA ADMINISTRATIVA; PROJETOS, ESTUDOS E NORMAS',
            'current_phase' => 'ENQUANTO VIGORA',
            'time_current_phase' => -1,
            'intermediate_phase' => '5 ANOS',
            'time_intermediate_phase' => 5,
            'final_destination' => 'GUARDA PERMANENTE',
            'observations' => ''
        ]);

        Classification::create([
            'cod'       => '002',
            'subject'   => 'PLANOS, PROGRAMAS E PROJETOS DE TRABALHO',
            'current_phase' => '5 ANOS',
            'time_current_phase' => 5,
            'intermediate_phase' => '9 ANOS',
            'time_intermediate_phase' => 14,
            'final_destination' => 'GUARDA PERMANENTE',
            'observations' => ''
        ]);   

        Classification::create([
            'cod'       => '003',
            'subject'   => 'RELATÓRIOS DE ATIVIDADES',
            'current_phase' => '5 ANOS',
            'time_current_phase' => 5,
            'intermediate_phase' => '9 ANOS',
            'time_intermediate_phase' => 14,
            'final_destination' => 'GUARDA PERMANENTE',
            'observations' => 'São passíves de eliminação os relatórios cujas informações encontram-se recaptuladas em outros'
        ]);

    }
}
