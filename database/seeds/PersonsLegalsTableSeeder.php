<?php

use Illuminate\Database\Seeder;
use App\Models\PersonLegal;
use Carbon\Carbon;

class PersonsLegalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('persons_legals')->insert([
            'cnpj' => '21.362.347/0001-54',
            'corporate_name'  => 'XTeste Ltda 3',
            'fantasy_name'  => 'INeedWebSite',
            'phone' => '+55 (85) 98566-8596',
            'address' => 'Av. Humberto Monte, 9883',
		    'observations' => '',
	        'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
