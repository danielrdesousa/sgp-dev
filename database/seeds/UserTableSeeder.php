<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name'                  => 'Administrador',
            'email'                 => 'admin@admin.com',
            'career'                => 'Estudante',
            'cpf'                   => '000.000.000-00',
            'date_birth'            => '2018/03/22',
            'observations'          => 'nothing observations',
            'password'  => bcrypt('admin'),
        ]);

    }
}
