<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('process_number')->unique();
            $table->string('subject');
            $table->string('date_create_month')->nullable();
            $table->string('date_create_year');
            $table->string('date_next_stage')->nullable();
            $table->integer('current_guard_stage'); 
            $table->integer('classifications_id')->unsigned();
            $table->foreign('classifications_id')->references('id')->on('classifications');
            $table->string('observations')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processes');
    }
}


/*
current_guard_stage:
    0-Encerrado
    1-Corrente
    2-Intermediaria
    3-Destinação Final
*/