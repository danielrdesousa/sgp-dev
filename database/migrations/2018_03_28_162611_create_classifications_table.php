<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('classifications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('cod')->unique();
            $table->string('subject');
            $table->string('current_phase')->nullable();
                $table->integer('time_current_phase')->nullable();
            $table->string('intermediate_phase')->nullable();
                $table->integer('time_intermediate_phase')->nullable();
            $table->string('final_destination')->nullable();
            $table->string('observations')->nullable();            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('classifications');
    }
}
