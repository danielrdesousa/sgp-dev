<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestedPhysicalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interested_physicals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('career')->nullable();
            $table->string('cpf')->unique()->nullable();
            $table->date('date_birth')->nullable();
            $table->string('registration_numbers')->nullable();
            $table->string('observations'); 
            $table->timestamps();      
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interested_physicals');
    }
}
