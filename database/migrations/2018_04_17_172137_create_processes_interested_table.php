<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcessesInterestedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processes_interested', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('processes_id')->unsigned()->nullable();
            $table->foreign('processes_id')->references('id')->on('processes')->onDelete('cascade');

            $table->integer('interested_physicals_id')->unsigned()->nullable();
            $table->foreign('interested_physicals_id')->references('id')->on('interested_physicals')->onDelete('cascade');


            $table->integer('interested_legals_id')->unsigned()->nullable();
            $table->foreign('interested_legals_id')->references('id')->on('interested_legals')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processes_interested');
    }
}
