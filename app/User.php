<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'career', 'cpf',
        'cnpj', 'date_birth', 'observations'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function insertUser($args){
        //create new obj User
        $user = new User;

        //fields for register
        $user->name = $args['username'];
        $user->email = $args['email'];
        $user->password = $args['password'];

        //verify if register is valid
        if ($user->save()){
            return [
                'success' => true,
                'message' => 'Usuário registrado com sucesso.'
            ];
        }
        else{
            return [
                'success' => false,
                'message' => 'Houve algum erro ao registrar o usuário!'
            ];          
        }
    }

    public function updateProfile($args)
    {
        //locate logged user
        $user = User::find(auth()->user()->id);

        //edit logged user own profile
        $response = $user->update([
                            'name' => $args['name'],
                            'date_birth' => $args['date_birth'],
                            'career' => $args['career'],
                            'cpf' => $args['cpf'],
                            'email' => $args['email'],
        ]);

        if ($response) {
            return [
                'success' => true,
                'message' => 'Perfil alterado com sucesso'
            ];
        }
        else{
            return [
                'success' => false,
                'message' => 'Houve algum erro ao alterar o perfil!'
            ];
        }
    }
}
