<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\User\StoreUser;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser $request, User $user)
    {
        $dataForm = $request->all();

        $response = $user->insertUser($dataForm);
        if ($response['success']) {
            return redirect()
                        ->route('user.index')
                        ->with('success', $response['message']);
        }else{
            return redirect()
                        ->back()
                        ->with('error', $response['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    { 
        $user = User::find($id);

        return view('admin.profile.index', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $destroy = $user->delete();

        if ($destroy) {
            return redirect()
                        ->route('list.users')
                        ->with('success', 'Deleted user successfully');
        }else{
            return redirect()
                        ->route('list.users')
                        ->with('error', 'There was an error deleting user');
        }
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showProfile()
    {
        return view('admin.profile.index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function editProfile()
    {
        return view('admin.profile.edit');
    }

    public function saveProfile(Request $request, User $method)
    {
        $dataForm = $request->all();

        $response = $method->updateProfile($dataForm);
        if ($response['success']) {
            return redirect()
                ->route('profile.index')
                ->with('success', $response['message']);
        }else{
            return redirect()
                ->back()
                ->with('error', $response['message']);
        }
    }

}
