<?php

namespace App\Http\Controllers;

use App\Models\Classification;
use Illuminate\Http\Request;
use App\User;

class ClassificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classifications = Classification::all();

        return view('admin.processes.classifications.index', compact('classifications'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.processes.classifications.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Classification $classification)
    {
        $dataForm = $request->all();
        $response = $classification->registerOrAlterClassification($dataForm, 0);

        if ($response['success']) {
            return redirect()
                        ->route('classification.index')
                        ->with('success', $response['message']);
        }else{
            return redirect()
                        ->back()
                        ->with('error', $response['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $classification = Classification::find($id);
        return view('admin.processes.classifications.edit', compact('classification'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classification $classification, $id)
    {
        $dataForm = $request->all();
        $response = $classification->registerOrAlterClassification($dataForm, $id);

        if ($response['success']) {
            return redirect()
                        ->route('classification.index')
                        ->with('success', $response['message']);
        }else{
            return redirect()
                        ->back()
                        ->with('error', $response['message']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $classification = Classification::find($id);
        $destroy = $classification->delete();

        if ($destroy) {
            return redirect()
                        ->route('classification.index')
                        ->with('success', 'Deleted classification successfully');
        }else{
            return redirect()
                        ->route('classification.index')
                        ->with('error', 'There was an error deleting classification');
        }
    }
}
