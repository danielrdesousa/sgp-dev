<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Process;
use Illuminate\Http\Request;
use App\Models\Classification;
use App\Http\Requests\Process\StoreProcess;


class ProcessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $processes = Process::all();

        return view('admin.processes.index', compact('processes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $classifications = Classification::all();
        return view('admin.processes.create', compact('classifications'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProcess $request, Process $process)
    {
        $dataForm = $request->all();

        //informações para teste
        $dataForm['interested'] = array( array(1,1), array(2,1));
        $dataForm['classification_id'] = 2;
        $dataForm['process_number'] = "23067.003732/2018-10";
        $dataForm['subject'] = "PASTA DO SERVIDOR";
        $dataForm['date_month'] = 0;
        $dataForm['date_year'] = 2015;
        $dataForm['observations'] = '';

        $response = $process->registerProcess($dataForm);
        if ($response['success']) {
            return redirect()
                        ->route('process.index')
                        ->with('success', $response['message']);
        }else{
            return redirect()
                        ->back()
                        ->with('error', $response['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $process = Process::find($id);
        
        return view('admin.processes.show', compact('process'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
}
