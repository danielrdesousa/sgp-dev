<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|max:255',
        ];
    }
    public function messages()
    {
        return [
            'username.required' => 'Por favor, insira um nome de usuário.',
            'username.string' => 'Por favor, insira um nome de usuário válido.',
            'email.required' => 'Por favor, insira o e-mail.',
            'email.email' => 'Por favor, insira um e-mail  válido.',
            'password.required' => 'Por favor, insira uma senha.',
        ];
    }
}
