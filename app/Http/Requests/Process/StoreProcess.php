<?php

namespace App\Http\Requests\Process;

use Illuminate\Foundation\Http\FormRequest;

class StoreProcess extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'process_number' => 'required|unique:processes,process_number|string|max:255',
            'subject' => 'required|string|max:255',
            // 'interested' => 'required|string|max:255',
            // 'date_create' => 'required|string|max:255',
            // 'current_guard_stage' => 'required|string|max:255',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'process_number.required' => 'Por favor, insira o número do processo.',
            'process_number.string' => 'Atenção, insira um número de processo válido.',
            'process_number.unique' => 'Atenção, o número do processo inserido já existe no banco de dados.',
            'subject.required' => 'Por favor, insira o assunto do processo.',
            // 'interested.required' => 'Por favor, insira o nome do interessado no processo.',
            // 'interested.string' => 'Por favor, insira um nome válido.',
            // 'date_create.required' => 'Por favor, insira a data de criação do processo.',
            // 'current_guard_stage.require' => 'Por favor, indique a fase do processo.',
        ];
    }
}
