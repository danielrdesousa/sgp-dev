<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Classification extends Model
{
    protected $fillable = [
        'cod', 'subject'
    ];

    public function registerOrAlterClassification($dataForm, $id)
    {
      if(!isset($id) || $id == 0){
        $classification = new Classification;
        $tipo = 1;
      }else{
        $classification = Classification::find($id);
        $tipo = 2;
      }

      $classification->cod = $classification->transformUppercase($dataForm['cod']);
      $classification->subject = $classification->transformUppercase($dataForm['subject']);
      $classification->final_destination = $classification->transformUppercase($dataForm['final_destination']);

      $classification->current_phase = $classification->transformUppercase($dataForm['current_phase']);
      $classification->intermediate_phase = $classification->transformUppercase($dataForm['intermediate_phase']);
		  $classification->time_current_phase = $classification->transformTextToYears($classification->transformUppercase($dataForm['current_phase']));
      $classification->time_intermediate_phase = $classification->transformTextToYears($classification->transformUppercase($dataForm['intermediate_phase'])) + $classification->time_current_phase;

      $classification->observations = $classification->transformUppercase($dataForm['observations']);

		  if ($classification->save()){
        if($tipo == 1){
          return [
            'success' => true,
            'message' => 'Classification successfully registered'
          ];
        }else{
          return [
            'success' => true,
            'message' => 'Classification changed successfully'
          ];          
        }
      }else{
        if($tipo == 1){
          return [
            'success' => false,
            'message' => 'There was an error registering classification'
          ];
        }else{
          return [
            'success' => false,
            'message' => 'There was an error changed classification'
          ];          
        }

      }
   	}

   	public function transformTextToYears($args)
    {
   		if($args==''){
   			return 0;
   		}else{
  			$year_value = preg_replace('/\D/', '', $args);
  			$aux = $year_value." ANOS";

  			if ($args == $aux || $year_value == $args) {
  			  return $year_value;
  			}else {
  			  return -1;
  			}
   		}
   	}

   	public function transformUppercase($args)
    {
   		return mb_strtoupper($args, 'UTF-8');
   	}
}