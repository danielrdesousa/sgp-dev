<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\InterestedPhysical;
use App\Models\InterestedLegal;
use App\Models\Classification;

class Process extends Model
{
    
    public function registerProcess($args)
    {
        $process = new Process;

        $process->process_number = $args['process_number'];
        $process->subject = $args['subject'];
        $process->classifications_id = $args['classification_id'];

    }

}

















































/*
    APAGAR ESTE CODIGO FUTURAMENTE


        // if ($process->save()){
        //     return [
        //         'success' => true,
        //         'message' => 'Process successfully registered'
        //     ];
        // }
        // else{
        //     return [
        //         'success' => false,
        //         'message' => 'There was an error registering process'
        //     ];          
        // }


    public function registerProcess($args){
        $process = new Process;
        $classification = Classification::find($args['classification']);

        // fields basics
        $process->process_number = $args['process_number'];
        $process->subject = $args['subject'];
        
        $process->classifications_id = $classification->id;
        $process->date_create = $args['date_create'];

        $data_next_stage_current = $process->createDateNextStage($classification->time_current_phase, $args['date_create']);
        $data_next_stage_intermediate = $process->createDateNextStage($classification->time_intermediate_phase, $args['date_create']);

        if($args['current_guard_stage'] == 1){
            if($classification->time_current_phase == -1){
                $process->current_guard_stage = 1;
                $process->date_next_stage = '';
            }elseif( ($classification->time_current_phase == 0 && $classification->time_intermediate_phase == 0) || $process->checkDateHasPassed($data_next_stage_intermediate) ){
                $process->current_guard_stage = 3;
                $process->date_next_stage = '';        
            }elseif( !$process->checkDateHasPassed($data_next_stage_intermediate) && $process->checkDateHasPassed($data_next_stage_current)  ){
                $process->current_guard_stage = 2;
                $process->date_next_stage = $data_next_stage_intermediate;
            }else{
                $process->current_guard_stage = 1;
                $process->date_next_stage = $data_next_stage_current;
            }
        }elseif ($args['current_guard_stage'] == 2) {
            if($classification->time_intermediate_phase == 0 || $process->checkDateHasPassed($data_next_stage_intermediate)  ){
                $process->current_guard_stage = 3;
                $process->date_next_stage = '';
            }else{
                $process->current_guard_stage = 2;
                $process->date_next_stage = $data_next_stage_intermediate;
            }
        }else{
            $process->current_guard_stage = 3;
            $process->date_next_stage = '';
        }

        if ($process->save()){
            return [
                'success' => true,
                'message' => 'Process successfully registered'
            ];
        }
        else{
            return [
                'success' => false,
                'message' => 'There was an error registering process'
            ];          
        }
    }


    public function createDateNextStage($years, $date_create){
        $aux_date = '+'.($years).' year';
        $date_next_stage = date('m/d/Y', strtotime($aux_date, strtotime($date_create)));
        return $date_next_stage;
    }

function checkDateHasPassed($date){
        $date_check = strtotime($date);
        $date_current = strtotime(date('m/d/Y'));
        if($date_current > $date_check){
            return true;
        }else{
            return false;
        }
    }
*/