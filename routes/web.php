<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function(){
	//urls for users
	Route::get('/users', 'UserController@index')->name('user.index');
		
	Route::get('/users/register', 'UserController@create')->name('user.create');

	Route::post('/users/register', 'UserController@store')->name('user.store');
		
	Route::get('/users/destroy/{id}', 'UserController@destroy')->name('user.destroy');

	//urls for profile
	Route::get('/profile', 'UserController@showProfile')->name('profile.index');

	Route::get('/profile/edit', 'UserController@editProfile')->name('profile.edit');

	Route::post('/profile/edit', 'UserController@saveProfile')->name('profile.save');

	//urls for processes
	Route::prefix('processes')->group(function () {
		//read
		Route::get('/', 'ProcessController@index')->name('process.index');
			
		//create
		Route::get('/register', 'ProcessController@create')->name('process.create');
			Route::post('/register', 'ProcessController@store')->name('process.store');
		
		//update
		Route::get('/details/{id}', 'ProcessController@show')->name('process.show');

		// CLASSIFICATIONS
			//read
		    Route::get('/classifications', 'ClassificationController@index')->name('classification.index');

		    //create
			Route::get('/classifications/register', 'ClassificationController@create')->name('classification.create');
				Route::post('/classifications/register', 'ClassificationController@store')->name('classification.store');

			//delete
			Route::get('/classifications/destroy/{id}', 'ClassificationController@destroy')->name('classification.destroy');

			//update
			Route::get('/classifications/edit/{id}', 'ClassificationController@edit')->name('classification.edit');
			Route::post('/classifications/edit/{id}', 'ClassificationController@update')->name('classification.update');
	});

	Route::prefix('interested')->group(function () {
		//url for search ajax
		Route::get('search/{data?}', 'InterestedController@search');
	});

});

Route::get('/', 'DashboardController@index')->name('homepage');
Auth::routes();